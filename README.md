# chatter - Redis Demo for INSY Presentation
Simple demo app to showcase Redis' Publisher/Subscriber model
> Author: Johannes Mayrhofer, 10.04.23

# Installation
## Docker
```sh
$ docker run --name chat-redis -p 6379:6379 -it redis
```

## Build application
```sh
$ go build -o chatter chatter.go
```

# Getting Started
## Join a chatroom
```sh
$ ./chatter --client johannes --join chat@home
....
...
chat@home: johannes wrote on 2023-04-11 20:00:00: hey fam!
...
....
```

## Chat in a room
```sh
$ ./chatter --client johannes --chat chat@home --message "hey fam!"
```