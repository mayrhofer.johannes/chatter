package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/redis/go-redis/v9"
)

type ChatRoomClient struct {
	*redis.Client
}

func main() {
	client := ""
	flag.StringVar(&client, "client", "", "nickname")
	message := ""
	flag.StringVar(&message, "message", "", "send this message to --chat")
	chatroom := ""
	flag.StringVar(&chatroom, "chat", "", "send --message to this chat")
	joinChat := ""
	flag.StringVar(&joinChat, "join", "", "join this chat")

	flag.Usage = func() {
		fmt.Printf("usage: %s --client c [--join chat | (--chat room --message msg)]\n", os.Args[0])
	}
	flag.Parse()

	c := redis.NewClient(&redis.Options{Addr: "192.168.82.176:6379", Password: "", DB: 0})
	defer c.Conn().Close()

	chat := &ChatRoomClient{c}
	if message != "" && chatroom != "" {
		chat.send(client, chatroom, message)
		return
	}
	if joinChat != "" {
		chat.join(joinChat)
		return
	}
}

func (self *ChatRoomClient) send(client, chatroom, message string) {
	must(self.Publish(context.Background(), chatroom, fmt.Sprintf("%s wrote on %s: %s", client, time.Now().Format("01-02-2006 15:04:05"), message)))
}

func (self *ChatRoomClient) join(chat string) {
	ctx := context.Background()
	sub := self.Subscribe(ctx, chat)
	iface, err := sub.Receive(ctx)
	if err != nil {
		panic(err)
	}

	switch iface.(type) {
	case *redis.Subscription:
		fmt.Println("successfully joined " + chat + "!")
	}

	for msg := range sub.Channel() {
		fmt.Printf("%s: %s\n", msg.Channel, msg.Payload)
	}
}

func must(status any) any {
	switch status.(type) {
	case redis.StatusCmd:
		if val, err := status.(*redis.StatusCmd).Result(); err == nil {
			return val
		} else {
			panic(err)
		}
	case redis.StringCmd:
		if val, err := status.(*redis.StringCmd).Result(); err == nil {
			return val
		} else {
			panic(err)
		}
	case redis.IntCmd:
		if val, err := status.(*redis.IntCmd).Result(); err == nil {
			return val
		} else {
			panic(err)
		}
	}
	return nil
}
